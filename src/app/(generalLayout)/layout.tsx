export default function GeneralLayout({ children }: { children: React.ReactNode }) {
  return (
    <main>
      <div>{children}</div>
    </main>
  )
}
