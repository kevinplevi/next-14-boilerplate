import { type SliceCreator } from "@/shared/store/type";

export interface SliceHome {
  numberCount: number;
  setNumberCount: (params: number) => void;
}

/**
 * Increment this if u change something, refer to semver. Also update the store.
 *
 * @see https://semver.org/
 * @version v1.0.0
 */
const createSliceHome: SliceCreator<SliceHome> = (set) => ({
  numberCount: 0,
  setNumberCount: (numberCount) => set(() => ({ numberCount })),
});

export default createSliceHome;
