import { create } from "zustand";

import createSliceHomeBlog, { type SliceHome } from "./slice-home-store";

type Store = SliceHome;

const useStoreHomeBlog = create<Store>((...params) => ({
  ...createSliceHomeBlog(...params),
}));

export default useStoreHomeBlog;
