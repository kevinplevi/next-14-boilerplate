import { StateCreator } from "zustand";

export type SliceCreator<T> = StateCreator<T>;
